--a
INSERT Clients VALUES
('Twilight', 'Sparkle', 'twilight.sparkle@gmail.com', '067-756-6514'),
('Rainbow', 'Dash', 'rainbow.dash1337@gmail.com', '067-548-5254'),
('Pinkie', 'Pie', 'pinkie.pie@gmail.com', '067-795-6221'),
('Rarity', 'Sorceress', 'rarity@gmail.com', '067-778-3215'),
('Jack', 'Apple', 'applejack@gmail.com', '067-899-4511'),
('Flutter', 'Shy', 'fluttershy@gmail.com', '067-545-4813')

--b
INSERT INTO Clients(FirstName, LastName, PhoneNumber) 
VALUES
('Larry', 'Wright', '812-204-7449'),
('Arlene', 'Ishmael', '916-708-5581'),
('Michael', 'Miner', '303-870-9137')

--c
INSERT INTO ClientsCopy
SELECT FirstName, LastName, Email, PhoneNumber
FROM Clients