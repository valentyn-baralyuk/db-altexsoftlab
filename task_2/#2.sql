--a
UPDATE Clients
SET FirstName = 'Phoenix'
WHERE Id = 7

--b
UPDATE Clients
SET Email = NULL
WHERE LEN(LastName) > 8

--c
UPDATE Products
SET Price = Price * 1.2
FROM Products JOIN Categories ON Products.CategoryId = Categories.Id
WHERE Categories.Category = 'Dairy'
