SELECT FirstName, LastName, Region, City
FROM Employees WHERE Region = 'WA'
EXCEPT SELECT FirstName, LastName, Region, City
FROM Employees WHERE City = 'Seattle'