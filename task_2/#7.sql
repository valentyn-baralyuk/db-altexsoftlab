SELECT City, Region, COUNT(SupplierID) Suppliers
FROM Suppliers
WHERE Country = 'USA'
GROUP BY City, Region
HAVING COUNT(SupplierID) > 1