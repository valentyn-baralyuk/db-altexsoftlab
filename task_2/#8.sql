SELECT 
	(SELECT CategoryName FROM Categories
	WHERE CategoryID = Products.CategoryID) AS CategoryName, 
	MAX(UnitPrice) AS MaxPrice
FROM Products
GROUP BY CategoryID