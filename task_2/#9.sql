SELECT E.FirstName, E.LastName, T.TerritoryDescription, R.RegionDescription
FROM Employees AS E
JOIN EmployeeTerritories AS ET
	ON E.EmployeeID = ET.EmployeeID
JOIN Territories AS T
	ON ET.TerritoryID = T.TerritoryID
JOIN Region AS R
	ON T.RegionID = R.RegionID