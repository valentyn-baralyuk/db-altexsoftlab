CREATE PROCEDURE [dbo].[InsertIds]
	@table AS [UD_TestTable]
AS BEGIN 
	DECLARE @id nvarchar(50)
	DECLARE @curId nvarchar(50)
	DECLARE @index nvarchar(50)
	SELECT @index = 1
	DECLARE id_cursor CURSOR
	FOR
	SELECT Id FROM TestTable
	OPEN id_cursor
 
	FETCH NEXT FROM id_cursor INTO @curId
 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC CreateUniqueId 'TEST_TABLE', @id OUTPUT
		UPDATE TestTable
		SET Id = @id + CONVERT(NVARCHAR(10), @index)
		WHERE CURRENT OF id_cursor
		SELECT @index = @index + 1
		FETCH NEXT FROM id_cursor INTO @curId
	END
	CLOSE id_cursor  
	DEALLOCATE id_cursor
END




