﻿CREATE PROC CreateUniqueId
    @tableName NVARCHAR(50),
    @id NVARCHAR(30) OUTPUT
AS
BEGIN
    SET @tableName = SUBSTRING (@tableName, 0, 6)
	SET @id = @tableName + ' ' + CONVERT(NVARCHAR(30), SYSDATETIME())
END