CREATE TRIGGER IOI_Trigger ON TestTable
INSTEAD OF INSERT 
AS BEGIN 
  DECLARE @Id nvarchar(30)
  EXEC CreateUniqueId 'Test', @Id OUTPUT
  INSERT TestTable
  SELECT Id = @Id, FirstName, LastName
  FROM inserted 
END
DROP TRIGGER IOI_Trigger