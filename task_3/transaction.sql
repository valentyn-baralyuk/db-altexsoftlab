CREATE TABLE Products
(
ProductId INT PRIMARY KEY,
ProductName VARCHAR(100),
Rate MONEY
) 

CREATE PROC usp_UpdateProductsData
	@Request XML
AS
BEGIN
	BEGIN TRANSACTION;  
	BEGIN TRY  
		MERGE Products AS TARGET
		USING (
			SELECT
				Item.value('ProductId[1]','int') AS ProductId,
				Item.value('ProductName[1]','varchar(100)') AS ProductName,
				Item.value('Rate[1]','money') AS Rate
			FROM
				@Request.nodes('/*/*') AS XmlData(Item)) AS SOURCE
		ON (TARGET.ProductId = SOURCE.ProductId) 
		WHEN MATCHED AND
			(TARGET.ProductName <> SOURCE.ProductName OR TARGET.Rate <> SOURCE.Rate) 
		THEN UPDATE 
			SET
				TARGET.ProductName = SOURCE.ProductName, 
				TARGET.Rate = SOURCE.Rate 
		WHEN NOT MATCHED BY TARGET THEN 
			INSERT (ProductId, ProductName, Rate) 
			VALUES (SOURCE.ProductId, SOURCE.ProductName, SOURCE.Rate)
		WHEN NOT MATCHED BY SOURCE THEN 
			DELETE;
	END TRY  
	BEGIN CATCH
		EXEC sp_XML_removedocument;
		ROLLBACK;  
	END CATCH;  
		COMMIT;
END


------------------DEMO------------------
DECLARE @req XML;
SET @req = 
'<Data>
	<Products>
		<ProductId>1</ProductId>
		<ProductName>Tea</ProductName>
		<Rate>21.00</Rate>
	</Products> 
	<Products>  
		<ProductId>2</ProductId>
		<ProductName>Cake</ProductName>
		<Rate>41.00</Rate>
	</Products>
</Data>'

SELECT * FROM Products
EXEC usp_UpdateProductsData @req
SELECT * FROM Products